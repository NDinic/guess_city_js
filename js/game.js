'use strict'
///*

const Game = (function () {
  'use strict';
  const cityInput = document.querySelector('#cityInput');
  const btnAddList = document.getElementById("btnAddList");
  const areaInner = document.querySelector('#area');
  const areaTimeInner = document.querySelector('#areaTime');
  const endGame = document.querySelector('#endGame');
  let addCityArr = [];

  const getArea = (area) => {
    areaInner.innerHTML = `<span>Oblast: ${area}</span>`
  }
  const cityAutoComplete = (dataP) => {
    const cities = dataP;
    cityInput.addEventListener("keyup", () => {
      // autocomplete
      const filteredArray = cities.filter(city => city.toLowerCase().startsWith(cityInput.value.toLowerCase()));
      let citySuggestion = '';

      filteredArray.map((el, i) => citySuggestion += `<span onClick="fillCityVal(${i})"> ${el} </span>`);

      cityAutocomplete.classList.add('active');
      cityAutocomplete.innerHTML = citySuggestion;

      window.fillCityVal = (i) => {
        cityAutocomplete.classList.remove('active');
        cityInput.value = filteredArray[i];
        cityAutocomplete.innerHTML = '';
        if (cities.filter(city => city.toLowerCase().includes(cityInput.value.toLowerCase()))) {
          cityInput.addEventListener('click', () => cityInput.value = '')
        }
      }
      // END citySuggestion
    })
  }

  //adding and removing from list
  // let addCityArr = [];
  let addCityToList = () => {
    (cityInput.value.length > 0 && !addCityArr.includes(cityInput.value.toLowerCase())) ? addCityArr.push(cityInput.value.toLowerCase()) : null;
    let citiesList = '';
    for (let i = 0; i < addCityArr.length; i++) {
      citiesList += `<li>${addCityArr[i].charAt(0).toUpperCase() + addCityArr[i].substring(1)}<span onClick="Game.deleteCity(${i})"> X</span></li>`;
      citiesListBlock.innerHTML = citiesList;
    }
  }

    let deleteCity = (el) => {
    console.log(el)
    if (addCityArr.filter(c => addCityArr.indexOf(c) == el)) {
      addCityArr.splice(el, 1);
      citiesListBlock.innerHTML = null;
      let citiesList = '';
      for (let i = 0; i < addCityArr.length; i++) {
        citiesList += `<li>${addCityArr[i].charAt(0).toUpperCase() + addCityArr[i].substring(1)}<span onClick="Game.deleteCity(${i})"> X</span></li>`;
        citiesListBlock.innerHTML = citiesList;
      }
    }
  }
  let btnAddListFunc = () => {
    btnAddList.addEventListener("click", function () {
      addCityToList();
    });
  }

  let timer = (areaTime, correctCities) => {
    (function () {
      let counting = true;
      setInterval(function () {
        if (counting) {
          if (areaTime >= 0) {
            areaTimeInner.innerHTML = `<span>Vreme: ${areaTime} sec </span>`;
            areaTime--;
          } else {

            clearInterval(timer);
            counting = false;
            finishGame(false, correctCities);
          }
        }
      }, 1000);
    })(areaTime)
  }

  // finis game
  let finishGame = (time, correctCities) => {

    let setItems = {
      'results': addCityArr,
      'correctCities': correctCities
    }
    let url = 'result.html?res=' + encodeURIComponent(setItems.results) + '&correct=' + encodeURIComponent(setItems.correctCities);
    document.location.href = url;
  }
  let endGameFunc = (correctCities) => {
    // console.log(correctCities)
    endGame.addEventListener('click', (e) => {
      e.preventDefault;
      finishGame(false, correctCities)
    })
  }

  const init = (data, resultCorrect, results) => {
    if (data) {
      cityAutoComplete(data.ponudjene);
      getArea(data.oblast);
      timer(data.vreme, data.tacno);
      // timer(10, data.tacno);
      btnAddListFunc();
      endGameFunc(data.tacno);
      console.log(data)
    }
    if(results) {
      showResult(resultCorrect, results)
      console.log('[resultCorrect] - ' + resultCorrect)
      console.log('[results] - ' + results)
    }
    
    
  }
  const showResult = (correctCities, addedCities) => {
    'use strict';
    const correctCitiesSum = document.querySelector('#correctCitiesSum');
    const addedCitiesWrap = document.querySelector('#addedCitiesWrap');
    const correctCitiesList = document.querySelector('#correctCitiesList');
    let correctAnswersArray = [];
    // const correctCities = result.correctCities;
    // const addedCities = result.results;
    let correctAnswers = 0;

    // console.log(addedCities.length);

    let checkAnswers = () => {
      for (let i = 0; i < addedCities.length; i++) {
        for (let j = 0; j < correctCities.length; j++) {
          if (correctCities[j].toLowerCase() === addedCities[i].toLowerCase()) {
            // console.log(addedCities[i]);
            correctAnswersArray.push(addedCities[i]);
            correctAnswers++;
          }
        }
      }
      correctCitiesSum.innerHTML = `<p>Broj pogodaka: ${correctAnswers}</p>`;
      addedCities.map(addedCity => addedCitiesWrap.innerHTML +=
        `<li>${addedCity.charAt(0).toUpperCase() + addedCity.substring(1)}</li>`);
      correctAnswersArray.map(correctCity => correctCitiesList.innerHTML +=
        `<li>${correctCity.charAt(0).toUpperCase() + correctCity.substring(1)}</li>`);

    }

    checkAnswers();

    // localStorage.clear();
  }
  return {
    init: init,
    deleteCity: deleteCity,
    showResult: showResult
  }
})()


//*/