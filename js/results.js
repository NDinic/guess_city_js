'use strict'

// import {Game} from './game.js'

window.onload = function () {
  var url = document.location.href,
      params = url.split('?')[1].split('&'),
      data = {}, tmp;
      // params = decodeURIComponent(params);

      //  console.log(params)
      // console.log(params.length)
  for (var i = 0, l = params.length; i < l; i++) {
  //  console.log(params)
       tmp = params[i].split('=');
       data[tmp[0]] = tmp[1];
  }
  const results = decodeURIComponent(data.res).split(',');
  const resultCorrect = decodeURIComponent(data.correct).split(',');
  // Game.showResult(resultCorrect, results)
  Game.init(null, resultCorrect, results)
}